from argparse import ArgumentParser


DEFAULT_START = 1415574042256
MAC_ADDRESS = "BC:20:A4:77:A9:39"
DEVICE_NAME = "Nexus 10"
RSSI = -70
times = [10, 20]


def write_row(f, time, user_appear, device_name, mac_address, rssi):
    f.write("%d\n" % (time))
    #print time
    if user_appear:
        t = "%s\t%s\t%d\n" % (device_name, mac_address, rssi)
        #print t
        f.write(t)

def scan(f, state, it):
  for x in it:
    state = f(state, x)
    yield state

def frange(x, y, jump):
  while x < y:
    yield x
    x += jump

def build_threshold(start, times):
    def increment(acc, curr):
        t, appear = acc
        return (t + curr, not appear)
    return scan(increment, (start, True), map(lambda x: x * 60 * 1000, times))


def increment_time(t):
    return t + 1500

def generate_trace(f, offsets, start_time, device_name, mac_address, rssi):
    curr = start_time
    total_time = sum(times)

    for threshold, appear in build_threshold(start_time, offsets):
        while curr < threshold:

            write_row(f, curr, appear, device_name, mac_address, rssi)
            curr = increment_time(curr)


#print list(build_threshold([0,3,4]))


def generate_trace_file(fname, offsets, start_time, device_name, mac_address, rssi):
    f = open(fname, "w")

    generate_trace(f, offsets, start_time, device_name, mac_address, rssi)
    f.close()

#generate_trace_file("test.txt", 0, [0, 10, 30])


def build_parser():
    parser = ArgumentParser("Generating virtual Bluetooth trace")
    
    parser.description = "It generates virtual Bluetooth trace. List of offsets(in minutes) will be interpreted like this.\n" +\
                         "'1 4 3 5' ==> 1 mintues bluetooth off  -> 5 mintues bluetooth on -> 8 minutes bluetooth off -> 13 mintues bluetooth on"

    parser.usage = "python bt_trace_gen.py 5 10 7"
    parser.add_argument("offsets", nargs='+', help='times in minutes. e.g. "5 10 7" -> 5 minutes bt off, 15 minutes on, 22 mintues off')

    parser.add_argument("--rssi", help="rssi value for generating trace. -75 by default", type=int, default=-75)
    parser.add_argument("--start-time", help="start timestamp(milliseconds) of the trace. 1415574042256 by default", type=int, default=DEFAULT_START)
    parser.add_argument("--device-name", help="device name for generating trace. %s by default"  % DEVICE_NAME, default=DEVICE_NAME)
    parser.add_argument("--mac-address", help="mac address for generating trace. %s by default" % MAC_ADDRESS, default=MAC_ADDRESS)
    parser.add_argument("--ouput", help="name for output file. 'ouput.txt' by default", default='output.txt')
    return parser

def main():
    p = build_parser()
    args = p.parse_args()
    #print args
    #print args.rssi
    try:
        ofs = map(int, args.offsets)
    except Exception as e:
        print "Error: Offsets should be list of integers.(e.g., '5 10 7')"
        exit(-1)
    generate_trace_file(args.ouput, ofs, args.start_time, args.device_name, args.mac_address, args.rssi)


main()
